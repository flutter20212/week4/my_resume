import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  Widget build(BuildContext context) {
    Widget titleSection = Container(
      padding: const EdgeInsets.all(32),
      child: Column(
        children: [
          Container(
            height: 200,
            width: 200,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(100.0),
              border: Border.all(
                width: 1.0,
                style: BorderStyle.solid,
              ),
              image: DecorationImage(
                fit: BoxFit.cover,
                image: AssetImage('images/profile.jpg'),
              ),
            ),
          ),
          Container(
            child: Column(
              children: [
                Text(
                  'Nopparat Wanwijit (Nung)',
                  style: TextStyle(
                    fontSize: 24,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                 Text(
                  'Tel.094-xxx-xxxx 61160257@go.buu.ac.th',
                  style: TextStyle(
                    color: Colors.blue[400],
                    fontWeight: FontWeight.w100,
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );

    Widget skillsSection = Container(
      child: Row(
        children: [
          Container(
            padding: EdgeInsets.all(20),
            child: Row(children: [
              Text('SKILLS',
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20)),
            ]),
          ),
          Container(
            padding: EdgeInsets.all(50),
            
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Image.asset('images/Java.png',
                    width: 70, height: 50, fit: BoxFit.fill),Text('      '),
                Image.asset('images/html.png',
                    width: 50, height: 50, fit: BoxFit.cover),Text('      '),
                Image.asset('images/Javascript.png',
                    width: 45, height: 50, fit: BoxFit.fill),Text('      '),
                Image.asset('images/Python.png',
                    width: 60, height: 50, fit: BoxFit.fill),Text('      '),
                Image.asset('images/css.png',
                    width: 35, height: 50, fit: BoxFit.fill),Text('      '),
                Image.asset('images/Mongo.png',
                    width: 50, height: 40, fit: BoxFit.cover),Text('      '),
                Image.asset('images/C.png',
                    width: 50, height: 40, fit: BoxFit.cover),Text('      '),
              ],
            ),
          )
        ],
      ),
    );
Column _buildButtonColumn(Color color, IconData icon, String label) {
  return Column(
    mainAxisSize: MainAxisSize.min,
    children: [
      Icon(
        icon,
        color: color,
      ),
      Container(
        child: Text(
          label,
          style: TextStyle(
            fontSize: 15,
            fontWeight: FontWeight.w400,
            color: color,
          ),
        ),
      ),
    ],
  );
}
Widget buttonSection = Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          _buildButtonColumn(
              Colors.blueAccent, Icons.male_rounded, 'Gender: male'),
          _buildButtonColumn(Colors.blueAccent, Icons.cake_rounded,
              'Birthday: 1 May 2000'),
          _buildButtonColumn(
              Colors.blueAccent, Icons.home, 'Address: 99/36 ChinBuri'),
          _buildButtonColumn(
              Colors.blueAccent, Icons.access_time, 'Free Time: Play Game'),
          _buildButtonColumn(
              Colors.blueAccent, Icons.tv, 'Like: anime cartoon'),
          _buildButtonColumn(
              Colors.blueAccent, Icons.border_color_rounded, 'Color: blue'),
        ],
      ),
    );
    Widget textSection = Container(
      padding: EdgeInsets.all(20),
      child:  Text('INFO',
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
        softWrap: true,
      ),
    );

    Widget profileSection = Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            padding: EdgeInsets.all(20),
            child: Text('PROFILE',
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20)),
          ),
          Container(
            padding: EdgeInsets.fromLTRB(32, 0, 20, 0),
            child: Text(
              '   My name is Nopparat Wanwijit. I’m from Thailand. '
              ' I live in Chonburi. '
              ' I’m twenty-one years old. '
              ' There are 4 people in my family. '
              ' My father, mother, sister and me, a total of 4 people. '
              ' We have a dog name is brownie ',
              softWrap: true,
            ),
          )
        ],
      ),
    );

    Widget educatioSection = Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            padding: EdgeInsets.all(20),
            child: Text('Education',
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20)),
          ),
          Container(
            padding: EdgeInsets.all(20),
            child: Row(
              children: [
                Column(
                  children: [
                    Image.asset(
                      'images/Buu.png',
                      width: 80,
                      height: 80,
                      fit: BoxFit.cover,
                    ),
                  ],
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.baseline,
                  textBaseline: TextBaseline.alphabetic,
                  children: [
                    Text(
                      '  2018 - Now',
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                    Text('  Currently studying at Buraphawittaya University Faculty of Information Science (Computer Science)',
                        style: TextStyle(color: Colors.red[400])),
                  ],
                )
              ],
            ),
          ),
          Container(
            padding: EdgeInsets.all(20),
            child: Row(
              children: [
                Column(
                  children: [
                    Image.asset(
                      'images/Angsila.png',
                      width: 80,
                      height: 90,
                      fit: BoxFit.cover,
                    ),
                  ],
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.baseline,
                  textBaseline: TextBaseline.alphabetic,
                  children: [
                    Text(
                      '  2015 - 1017',
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                    Text('  High School, Angsilawittayakhom School, Science-Mathematics',
                        style: TextStyle(color: Colors.red[400])),
                  ],
                )
              ],
            ),
          ),
        ],
      ),
    );

    return MaterialApp(
      title: 'My_Resume',
      home: Scaffold(
        appBar: AppBar(
          title: const Text('My_Resume'),
        ),
        body: ListView(children: [
          titleSection,
          skillsSection,
          profileSection,
          textSection,
          buttonSection,
          educatioSection
        ]),
        backgroundColor: Colors.amberAccent[100],
      ),
    );
  }
}
